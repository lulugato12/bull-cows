defmodule BullsAndCows do
  def bulls([], []), do: []
  def bulls(secret, guess) do
    if (hd(secret) == hd(guess)) do
      [hd(secret)] ++ BullsAndCows.bulls(tl(secret), tl(guess))
    else
      BullsAndCows.bulls(tl(secret), tl(guess))
    end
  end

  def cows(_secret, []), do: 0
  def cows(secret, guess) do
    if (Enum.member?(secret, hd(guess))) do
      BullsAndCows.cows(secret, tl(guess)) + 1
    else
      BullsAndCows.cows(secret, tl(guess))
    end
  end

  def score_guess(secret, guess) do
    sl = String.codepoints(secret)
    gl = String.codepoints(guess)
    b = BullsAndCows.bulls(sl, gl)
    c = BullsAndCows.cows(sl -- b , gl)

    if (Enum.count(b) == 4) do
      "You win"
    else
      "#{Enum.count(b)} Bulls, #{c} Cows"
    end
  end
end
